package Game;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Deck {

    private final String[] palo = {"corazones", "diamantes", "trébol", "picas"};
    private final String[] color = {"rojo", "negro"};
    private final String[] valor = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "A", "J", "Q", "K"};

    private ArrayList<Card> deck;

    public Deck() {
        deck = new ArrayList<Card>();

        for (int palo = 0; palo < 2; palo++)

            for (int valor = 0; valor < this.valor.length; valor++)

                deck.add(new Card(this.palo[palo], color[0], this.valor[valor]));

        for (int palo = 2; palo < 4; palo++)

            for (int valor = 0; valor < this.valor.length; valor++)

                deck.add(new Card(this.palo[palo], color[1], this.valor[valor]));
    }

    public int getSize() {
        return deck.size();
    }

    public ArrayList<Card> getDeck() {
        return deck;
    }

    public void suflle() {
        Collections.shuffle(deck);
        System.out.println("Se mezclo el Deck.");

        for (Card elemento : deck) {
            System.out.print(elemento + " / ");
        }
    }

    public void head(){
        System.out.println(deck.get(0));
        deck.remove(0);
        System.out.println("Quedan " + deck.size() + " cartas");
    }


    public void pick() {
        Random random = new Random();
        System.out.println(
                deck.get(
                        random.nextInt(
                                deck.size()
                        )
                )
        );

        deck.remove(random.nextInt(deck.size()));
        System.out.println("Quedan " + deck.size() + " cartas");
    }

    public void hand() {
        for(int i = 0; i < 5; i++){
            System.out.println(deck.get(i));
        }
        for(int i = 0; i < 5; i++){
            deck.remove(i);
        }
        System.out.println("Quedan " + deck.size()+ " cartas");
    }
}